import itertools
import re
import sys

from concurrent.futures import ThreadPoolExecutor, Future
from datetime import datetime

import requests
import arrow

from lxml.cssselect import CSSSelector
from lxml import html

try:
    from sopel import module, tools, formatting
except ImportError:

    class Useless:
        def __getattr__(self, name):
            return None

    module = bot = formatting = Useless()
    module.rule = lambda x: (lambda y: y)
    bot.say = print
    formatting.color = lambda x, *args, **kwargs: x
    formatting.bold = lambda x: x
    formatting.colors = Useless()

TWITTER_URL_PATTERN = re.compile(r"(https?://(?:mobile.)?twitter.com/\w+/status/(?P<id>\w+))")

MAIN_SCRIPT_PATTERN = re.compile(
    r"https://abs.twimg.com/responsive-web/(?:client-web(?:-legacy)?|web|web_legacy)/main\.\w+\.js$"
)
BEARER_TOKEN_FROM_SCRIPT_PATTERN = re.compile(r'"AAAAAAA.+?"')
GUEST_TOKEN_FROM_SCRIPT_PATTERN = re.compile(r"gt=(.*?);")

BEARER_TOKEN_PATTERN = re.compile(r'"Bearer .*?"')

# Map script URI with known bearer tokens
BEARER_TOKENS = {}

# Map bearer tokens with guest tokens
GUEST_TOKENS = {}


def setup(bot):
    if not bot.memory.contains("url_callbacks"):
        bot.memory["url_callbacks"] = tools.SopelMemory()
    bot.memory["url_callbacks"][TWITTER_URL_PATTERN] = show_tweet


def get_bearer_token(script_uri, api_version=1):
    bearer_token = BEARER_TOKENS.get(script_uri, None)
    if bearer_token is not None:
        return bearer_token

    script = requests.get(script_uri)
    pattern = (
        BEARER_TOKEN_PATTERN if api_version == 1 else BEARER_TOKEN_FROM_SCRIPT_PATTERN
    )
    match = pattern.search(script.text)
    if match:
        # Remove quotes
        bearer_token = match.group(0)[1:-1]
        BEARER_TOKENS[script_uri] = bearer_token

    return bearer_token


def get_guest_token(bearer_token):
    guest_token = GUEST_TOKENS.get(bearer_token, None)
    if guest_token is not None:
        return guest_token

    activation = requests.post(
        "https://api.twitter.com/1.1/guest/activate.json",
        headers={"authorization": bearer_token,},
    )
    guest_token = activation.json().get("guest_token", None)

    return guest_token


def get_video_details(status_id, bearer_token, guest_token):
    details = requests.get(
        f"https://api.twitter.com/1.1/videos/tweet/config/{status_id}.json",
        headers={"authorization": bearer_token, "guest_token": guest_token,},
    )
    return details.json()


def get_video_uri(status_id):
    iframe = requests.get(f"https://twitter.com/i/videos/{status_id}")
    tree = html.fromstring(iframe.content)
    script_uri = CSSSelector("body > script")(tree)[0].get("src")

    bearer_token = get_bearer_token(script_uri)
    if bearer_token:
        guest_token = get_guest_token(bearer_token)
        if guest_token:
            details = get_video_details(status_id, bearer_token, guest_token)
            return details.get("track", {}).get("playbackUrl", None)


def get_real_url(url):
    try:
        return requests.head(url, allow_redirects=True).url
    except Exception:
        return url


def parse_tweet_old(tweet_element):
    tweet = {}
    # Remove useless links
    for element in CSSSelector(".twitter-timeline-link.u-hidden")(tweet_element):
        element.drop_tree()

    link_elements = CSSSelector(".twitter-timeline-link")(tweet_element)
    for link_element in link_elements:
        url = link_element.get("data-expanded-url") or ""
        url_text_element = html.Element("p")
        url_text_element.text = " {} ".format(get_real_url(url))
        link_element.addnext(url_text_element)
        # Remove it
        link_element.drop_tree()

    for emoji_element in CSSSelector(".Emoji")(tweet_element):
        emoji_text_element = html.Element("p")
        emoji_text_element.text = emoji_element.get("alt") or ""
        emoji_element.addnext(emoji_text_element)
        # Remove
        emoji_element.drop_tree()

    tweet["text"] = CSSSelector(".tweet-text")(tweet_element)[0].text_content()

    image_elements = CSSSelector("img:not(.avatar)")(tweet_element)
    tweet["images"] = [
        {"url": "{}:large".format(element.get("src"))} for element in image_elements
    ]

    video_elements = CSSSelector("div.AdaptiveMedia-video")(tweet_element)
    tweet["videos"] = []
    if video_elements:
        video_uri = get_video_uri(tweet_element.get("data-tweet-id"))
        if video_uri:
            tweet["videos"] += [{"url": video_uri}]

    tweet["retweet_count"] = CSSSelector("span.ProfileTweet-action--retweet > span")(
        tweet_element
    )[0].get("data-tweet-stat-count")

    tweet["user"] = {}
    tweet["user"]["name"] = tweet_element.get("data-name")

    # Bad hardcoded domain
    tweet["permalink"] = "https://twitter.com{}".format(
        tweet_element.get("data-permalink-path")
    )

    timestamp = CSSSelector("span.js-short-timestamp")(tweet_element)[0].get(
        "data-time"
    )
    tweet["date"] = arrow.get(int(timestamp))
    tweet["date"].humanized = tweet["date"].humanize(locale="fr")

    return tweet


def extract_tweet(url):
    session = requests.Session()
    # Twitter needs a "real" browser.
    session.headers.update({'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0'})
    page = session.get(url)
    tree = html.fromstring(page.content)

    links = tree.head.findall("link")
    link = next(
        (l for l in links if MAIN_SCRIPT_PATTERN.match(l.attrib.get("href") or "")),
        None,
    )
    if link is None:
        raise RuntimeError("Unable to find main script")

    main_script_uri = link.attrib["href"]
    bearer_token = get_bearer_token(main_script_uri, 2)

    guest_token = session.cookies.get("gt")
    if guest_token is None:
        # Nothing in cookies, so let's extract from document
        scripts = tree.body.findall("script")
        script = next((s for s in scripts if s.text is not None and "document.cookie =" in s.text), None)
        if script is None:
            raise RuntimeError("Unable to find guest token")

        guest_token = GUEST_TOKEN_FROM_SCRIPT_PATTERN.search(script.text).group(1)

    tweet_id = TWITTER_URL_PATTERN.search(url).group("id")
    details = session.get(
        f"https://api.twitter.com/2/timeline/conversation/{tweet_id}.json",
        headers={
            "authorization": f"Bearer {bearer_token}",
            "x-guest-token": guest_token,
        },
        params={
            "include_profile_interstitial_type": "1",
            "include_blocking": "1",
            "include_blocked_by": "1",
            "include_followed_by": "1",
            "include_want_retweets": "1",
            "include_mute_edge": "1",
            "include_can_dm": "1",
            "include_can_media_tag": "1",
            "skip_status": "1",
            "cards_platform": "Web-12",
            "include_cards": "1",
            "include_ext_alt_text": "true",
            "include_reply_count": "1",
            "tweet_mode": "extended",
            "include_entities": "true",
            "include_user_entities": "true",
            "include_ext_media_color": "true",
            "include_ext_media_availability": "true",
            "send_error_codes": "true",
            "simple_quoted_tweet": "true",
            "count": "20",
            "ext": "mediaStats%2ChighlightedLabel%2CcameraMoment",
            "include_quote_count": "true",
        },
    )

    details = details.json()

    tweet = details["globalObjects"]["tweets"][tweet_id]
    parse_tweet(tweet, details)
    replied_tweet = details["globalObjects"]["tweets"].get(
        tweet.get("in_reply_to_status_id_str")
    )
    quoted_tweet = details["globalObjects"]["tweets"].get(
        tweet.get("quoted_status_id_str")
    )
    if replied_tweet is not None:
        tweet["in_reply_to"] = parse_tweet(replied_tweet, details)

    if quoted_tweet is not None:
        tweet["quoted_status"] = parse_tweet(quoted_tweet, details)

    return tweet


def parse_tweet(tweet, details):
    tweet["created_at"] = arrow.get(tweet["created_at"], "ddd MMM DD HH:mm:ss Z YYYY")
    tweet["created_at"].humanized = tweet["created_at"].humanize(locale="fr")
    tweet["user"] = details["globalObjects"]["users"][tweet["user_id_str"]]
    tweet[
        "tweet_url"
    ] = f"https://twitter.com/{tweet['user']['screen_name']}/status/{tweet['id_str']}"

    for ((begin, end), urls) in get_media_by_indices(tweet):
        tweet["full_text"] = (
            tweet["full_text"][:begin] + " ".join(urls) + tweet["full_text"][end:]
        )

    # Remove newlines
    tweet["full_text"] = " – ".join(
        line for line in tweet["full_text"].split("\n") if line
    )
    return tweet


def get_media_by_indices(tweet):
    all_media = {}
    with ThreadPoolExecutor() as executor:
        for media in itertools.chain(
            tweet["entities"].get("urls", []),
            tweet.get("extended_entities", {}).get("media", []),
        ):
            (begin, end) = media["indices"]
            real_url = media.get("media_url_https", media["expanded_url"])
            if media.get("type") == "video":
                videos = [
                    v
                    for v in media["video_info"]["variants"]
                    if v.get("content_type", "").startswith("video")
                ]
                videos.sort(key=lambda video: video.get("bitrate"), reverse=True)
                if videos:
                    real_url = videos[0]["url"]
            elif "url" in media:
                # URL, try to follow redirections
                real_url = executor.submit(get_real_url, real_url)

            all_media.setdefault((begin, end), []).append(real_url)

    for indices in sorted(all_media.keys(), reverse=True):
        yield (
            indices,
            [
                (media.result() if isinstance(media, Future) else media)
                for media in all_media[indices]
            ],
        )


def format_tweet(tweet, truncate_text=False, show_url=False):
    when = formatting.color(
        tweet["created_at"].humanized, formatting.colors.GREY, formatting.colors.CYAN,
    )
    text = (
        tweet["full_text"]
        if not truncate_text or len(tweet["full_text"]) < truncate_text
        else f"{tweet['full_text'][:truncate_text]} […]"
    )
    username = formatting.bold(tweet["user"]["name"])

    url = f" @ {tweet['tweet_url']}" if show_url else ""
    return f"{when} - {username}: \"{text}\" ({tweet['retweet_count']} RT){url}"


@module.rule(".*{}.*".format(TWITTER_URL_PATTERN.pattern))
def show_tweet(bot, trigger, match=None):
    match = match or trigger
    tweet = extract_tweet(match.group(1).replace('://mobile.', '://'))

    if not tweet:
        bot.say("There is no tweet here")
        return

    bot.say(format_tweet(tweet))

    quoted_tweet = tweet.get("quoted_status")
    if quoted_tweet is not None:
        bot.say("Quoting: " + format_tweet(quoted_tweet, 200, True))
    replied_tweet = tweet.get("in_reply_to")
    if replied_tweet is not None:
        bot.say("In reply to: " + format_tweet(replied_tweet, 200, True))


"""
Tests

Multiple photos: https://twitter.com/JeremieJkbcz/status/1269240659199787009
Link with broken redirection: https://twitter.com/GRegaudie/status/1268547639731240969
Video: https://twitter.com/AGTourFR/status/1268472150933069825
Multiple links and image: https://twitter.com/Bfowah/status/1269078750047531010
Reply to the previous: https://twitter.com/Proulxes/status/1269232653204697088
Quoting tweet: https://twitter.com/ramcq/status/1278817807166255106

"""

if __name__ == "__main__":
    match = TWITTER_URL_PATTERN.search(sys.argv[1])
    show_tweet(bot, match)
    # tweet = extract_tweet(sys.argv[1])
    # print(tweet)
