Sopel Twitter - No API
==================
This is a module for [Sopel](https://sopel.chat/) similar to [sopel-twitter](https://github.com/sopel-irc/sopel-twitter) except that you don't need an API key.

Requirements
------------------

  - [sopel](https://github.com/sopel-irc/sopel) - Hey, what did you expect?
  - [requests](https://github.com/kennethreitz/requests) - The famous one
  - [arrow](https://github.com/crsmithdev/arrow) - Manage your dates
  - [lxml](http://lxml.de/) - Of course we need to parse some HTML
  - [cssselect](https://github.com/scrapy/cssselect) - I don't want to use XPath

Configuration
------------------
None for now. How cool is that?